import { Injectable } from '@nestjs/common';

@Injectable()
export class TemperatureService {
  convert(celcius: number) {
    return {
      celcius: celcius,
      fahrenheit: (celcius * 9.0) / 5 + 32,
    };
  }
}
